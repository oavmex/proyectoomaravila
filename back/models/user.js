'use strict'
//Modelación del objeto USUARIO
//Antes de guardar el usuario se encripta el PASSWORD
//Librerias bcrypt-nodejs y crypto
const mongoose        = require('mongoose')
const Schema          = mongoose.Schema
const bcrypt          = require('bcrypt-nodejs');
const crypto          = require('crypto');
var UserSchema  = new Schema ({
                  email       :{type: String, unique : true, lowercase: true},
                  displayName :String,
                  avatar     :String,
                  password   :{type: String, required: true},
                  signupDate :{type: Date, default: Date.now()},
                  lastLogin  : Date
                })


//Funciones que se utilizarán antes o despúes de que el modelo se guarde en la base de datos
/*
//La siguiente función encriptará la contraseña que introduzca el usuario antes de que se guarde
UserSchema.pre('save', (next)=>{
  let user = this;
  //Si el usuario no ha modificado su contraseña, termina la función y continúa con el middleware
//  if (!user.isModified('password')) return next()
  //To hash a password: Technique 1 (generate a salt and hash on separate function calls):
  bcrypt.genSalt(10,(err,salt)=>{
    if (err) return next(err);
    bcrypt.hash(user.password, salt, null, (err,hash) => {
      if (err) return next(err);
      user.password = hash
      next()
    })
  })
})
          ------------------------------------------------------------------
//La siguiente función encriptará la contraseña que introduzca el usuario antes de que se guarde
// if (!user.isModified('password')) return next()
//To hash a password: Technique 1 (generate a salt and hash on separate function calls):
UserSchema.pre('save', function (next) {
  if (!this.isModified('password')) return next() //Si el usuario no ha modificado su contraseña, termina la función y continúa con el middleware
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err)
    bcrypt.hash(this.password, salt, null, (err, hash) => {
      if (err) return next(err)
      this.password = hash
      next()
    })
  })
})
*/
UserSchema.pre('save', function(next){
  let user = this;
  if(!user.isModified('password')) return next()

  bcrypt.genSalt(10, (err, salt)=>{
    if(err) return next(err)

    bcrypt.hash(user.password, salt, null, (err, hash) =>{
      if (err) return next(err)

      user.password = hash
      next();
    });
  });
});
/*
UserSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return callback(err)
        callback(null, isMatch)
    })
}
*/
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

//Se utiliza la WEB de GRAVATAR donde, a partir de un email nos devuelve una avatar
//Gravatar (una abreviación para Globally Recognized Avatar en inglés, o avatar reconocido mundialmente, en español)
//es un servicio que ofrece un avatar único globalmente. El servicio fue creado por Tom Werner.
/*
UserSchema.methods.gravatar = function (size) {
  if (!size) {
    size = 200;
  }
  if (!this.email) return `https:/gravatar.com/avatar/?s${size}&d=retro`
  const md5 = crypto.createHash('md5').update(this.email).digest('hex')
  return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`
}
*/

UserSchema.methods.gravatar = function (){
  //Si no tiene un email registrado, retorna un AVATAR por default
  if(!this.email) return 'https://gravatar.com/avatar/?s=200&d=retro'
  //Si no tiene un email registrado, retorna un AVATAR por default
  //Si tiene correo, es posible que tenga un GRAVATAR, Se crea una función md5 a partir del email del usuario
  const md5 = crypto.createHash('md5').update(this.email).digest('hex');
  return `https://gravatar.com/avatar/${md5}?s=200&d=retro`;
}

module.exports = mongoose.model('User', UserSchema);
