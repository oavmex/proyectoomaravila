'use strict'
const service = require('../services') //Se obtiene parámetro  SECRET TOKEN
//Se crea función
function isAuth (req, res, next) {
  //Se comprueba que en el HEADERS de la petición exista un campo authorization
  if (!req.headers.authorization){
    //Envía mensaje de error y se sale del middleware y no continua
    return res.status(403).send({message:`No tienes autorización`})
  }
  //Si existe, se toma la variable TOKEN de la cabecera
  //El split es para desglozar el TOKEN, la cabecera de autorización incluye un texto peare
  //La cabecer se convierte en un arreglo con tantos elementos como espacios existen. En este caso sólo hay un espacio
  const token = req.headers.authorization.split(" ")[1]


  //Se manda llamar la Promesa de SERVICE
  service.decodeToken(token)
  .then(response =>{
    req.user = response
    next()
  })
  .catch(response => {
    res.status(response.status)
  })

}
module.exports = isAuth;
