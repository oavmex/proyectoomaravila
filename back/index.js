'use strict'

//variables del sistema
const app      = require('./app');
const mongoose = require('mongoose');
const config   = require('./config');

mongoose.connect(config.db,(err,res) => {
  if (err) {
  return console.log(`Error al conectarse a la base de datos : ${err}`)
  }
  console.log('Conexión a la BD establecida utilizando (mongoose)')
})
app.listen(config.port, () => {
  console.log(`Funcionando en http://localhost: ${config.port}` )
});
