'use strict'
//variables del sistema
const express     = require('express')
const app         = express()
const bodyParser  = require('body-parser')
const mongoose    = require('mongoose')
const Usuarios    = require ('../models/user')
const userctrl    = require('../controllers/userctrl');
const apiUser     = express.Router();
// ----------------------------------------------------------------
const requestJson=require('request-json');
var urlMlabRaiz = 'https://api.mlab.com/api/1/databases/techu-mx-oav/collections';
var apiKey = "apiKey=OGFoUS0EOk6OqYLmzGd2rHhVlnk7ZV-z"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)

//Se interpetan los Json con bodyParser
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json())

//console.log(body)
//console.log(clienteMlab)

//Consultamos un email
function consultaLogin (req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz
    + '/usuarios?q='
    +'{ $and: [ {"email":'  +'"'+ req.headers.email +'"}' + ', { "password":'+'"' + req.headers.password +'" }  ] }'
    + '&f={"id_usuario":1, "_id": 0}'
    + '&'+ apiKey );
      clienteMlab.get('', function(err, resM, body) {

        //Si no hay registros retornamos mensaje de no existe
        if ( body.length ==  0 )
        {
          //console.log("NO Valido")
          return res.send(body)
        }

        //Si hay registros
        var coleccionesUsuario = []
        if (!err) {
          for (var i = 0; i < body.length; i++) {
            if (body[i] != "system.indexes") {
              coleccionesUsuario.push(body[i])
            }
          }
          //console.log(coleccionesUsuario)
          res.send(coleccionesUsuario)
        }
        else {
          res.send(err)
        }

      });
  }

//Alta de un usuario
function altaLogin (req, res) {
  var registro =
  {
    "id_usuario": req.headers.id_usuario,
    "nombre": req.headers.nombre,
    "apellido": req.headers.apellido,
    "email": req.headers.email,
    "password": req.headers.password,
    "pais": req.headers.pais,
    "estado": false
   }
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', registro, function(err, resM, body) {
    //if (err) res.status(500).send({message: `Erro al salvar en la BD:${err}`})
    if (err) {
//sconsole.log(err)
      res.send(err)
    }
//console.log(body)
    res.send(body)
    //res.status(200).send(JSON.stringify(body))
  });
}

//Modifica  un usuario
function modificaLogin (req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
  clienteMlab.put('?q={"id_usuario": "' + req.params.id_usuario + '"}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
  //res.send(body)
  res.status(200).send(JSON.stringify(body))
  });
}

//Borra  un Login
function borraLogin (req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
  clienteMlab.put('?q={"id_usuario": "' + req.params.id_usuario + '"}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
  //res.send(body)
  res.status(200).send(JSON.stringify(body))
  });
}

//**********************************MONGOOSE****************************************
//Consultamos todos los usuarios
function getUsuarios (req, res) {
  Usuarios.find({}, (err, usuarios )=>{
    if (err) return res.status(500).send({message:`Error al consultar el ID: ${err}`})
    if (!usuarios) return res.status(404).send({message:`No existen usuarios`})
    res.status(200).send({Usuarios: usuarios})
  })
};
//Consultamos un usuario por ID
function getUsuario (req, res) {
  let id = req.params.id
  Usuarios.findById(id, (err, usuarios )=>{
    if (err) return res.status(500).send({message:`Error al consultar el ID: ${err}`})
    if (!usuarios) return res.status(404).send({message:`No existe el ID: ${id}`})
    res.status(200).send({Usuarios: usuarios})
  })
};
//Agregamos un nuevo usuario
function insertaUsuario(req,res) {
  console.log('POST /api/usuarios')
  console.log(req.body)
    console.log(req.headers)
  let usuarios = new Usuarios()
  usuarios.id_usuario = req.body.id_usuario
  usuarios.nombre     = req.body.nombre
  usuarios.apellido   = req.body.apellido
  usuarios.email      = req.body.email
  usuarios.password   = req.body.password
  usuarios.pais       = req.body.pais
  usuarios.save((err,usuarioStored)=>{
    if (err) res.status(500).send({message: `Erro al salvar en la BD:${err}`})
    res.status(200).send({Usuarios:usuarioStored})
  })
};
//Actualiza un usuario
function updateUsuario(req,res){
  let id     = req.params.id
  let updateId = req.body
  Usuarios.findByIdAndUpdate(id, updateId , (err, usuariosUpdated )=>{
    if (err) res.status(500).send({message:`Error al actualizar el ID: ${err}`})
    res.status(200).send({Usuarios: usuariosUpdated})
  })
};
//Borramos un ID
function deleteUsuario (req,res) {
  let id = req.params.id
  Usuarios.findById(id, (err , usuarios)=>{
    if (err) res.status(500).send({message:`Error al borrar el ID: ${err}`})
    usuarios.remove(err => {
      if (err) res.status(500).send({message:`Error al borrar el ID: ${err}`})
      res.status(200).send({message: 'El usuario ha sido borrado'})
    })
  })
};


module.exports = {
  consultaLogin,
  altaLogin,
  modificaLogin,
  borraLogin,
  getUsuario    ,
  getUsuarios   ,
  insertaUsuario,
  updateUsuario ,
  deleteUsuario
}
