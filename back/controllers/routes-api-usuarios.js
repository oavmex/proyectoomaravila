'use strict'
const express     = require ('express');
const apiUsuarios = require('../controllers/api-usuarios');
const userctrl    = require('../controllers/userctrl');
const auth        = require('../middlewares/auth');
const apiUser     = express.Router();
const cuentas     = require ('../controllers/api-cuentas')

/*------------- Funciones de API USUARIOS ---------------------------*/
//Consulta un Login con Email
apiUser.get('/usuarios/login', apiUsuarios.consultaLogin);
//Inserta un Login
apiUser.post('/usuarios/alta', apiUsuarios.altaLogin);
//Actualiza un Login
apiUser.put('/usuarios/modifica/:id_usuario', apiUsuarios.modificaLogin);
//Borramos un Login
apiUser.put('/usuarios/borra/:id_usuario', apiUsuarios.borraLogin);

//Consulta una cuenta de un usuario
apiUser.get('/cuenta/consulta', cuentas.consultaCuenta);


//Consultamos todos los usuarios
apiUser.get('/usuarios', apiUsuarios.getUsuarios);
//Consultamos un usuario por ID
apiUser.get('/usuarios/:id', apiUsuarios.getUsuario);
//Agregamos un nuevo usuario
//apiUser.post('/usuarios', auth, apiUsuarios.insertaUsuario);
//Actualizamos un usuario por ID
apiUser.put('/usuarios/:id', auth, apiUsuarios.updateUsuario);
//Borramos un ID
apiUser.delete('/usuarios/:id', auth, apiUsuarios.deleteUsuario);

apiUser.post('/signup', userctrl.signUp);
apiUser.post('/signin', userctrl.signIn);
//RUTA PRIVADA
apiUser.get('/private', auth, (req,res)=>{
 res.status(200).send({message:'Tienes acceso'})
})

module.exports = apiUser ;
