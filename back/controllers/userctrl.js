'use strict'
//Registro y autenticación de usuarios
//Para que un usuario se pueda autenticar y registrar:
//No se utiliza PassPortJS a fin de que pueda ser escalable a cualquier FRONT se utilizó Tokens
//Se utilizan JSON TOKENs porque el usario envía un código al server y este lo descifra y valida que esté registrado
//permite no guardar sesiones y la lógica cae del lado del cliente
//JSON WEB TOKENS contiene 3 partes (HEADER, PAYLOAD y VERIFY SIGNATURE)

const User    = require('../models/user') //Se importa la Modelación del objeto USUARIO
const service = require('../services') //Se importa el servicio que gestiona el TOKEN

/*
//Función del registro usario
function signUp (req,res) {
  //Se crea el usuario, del modelo de usuario se utiliza email y nombre. La contraseña se genera con bcrypt en el modelo del usuario
  const user = new usuarios({
    email       :req.body.email,
    displayName :req.body.displayName,
    password    :req.body.password
  })
  user.avatar = user.gravatar();
  const signUp = (req, res) => {
    const user = new User({
      email: req.body.email,
      displayName: req.body.displayName,
      password: req.body.password
    })
    user.avatar = user.gravatar();
  //Se guarda el usuario
  user.save((err) => {
    //Se valida si se creó correctamente
    if(err) return res.status(500).send({message: `Error al crear el usuario: ${err}`})

    //Si no hay error, se contesta con un TOKEN, nota: Se utiliza un service por separado
    //a fin de no tenerlo en este archivo por si se utiliza esta funcionalidad en otro sitio
    return res.status(200).send({token: service.createToken(user)})
  })
}
*/
//console.log(req.header.email)

const signUp = (req, res) => {
  const user = new User({
    //email       : req.body.email,
    //password    : req.body.password
    email       : req.body.email,
    password    : req.body.password
  })
  user.avatar = user.gravatar();
  user.save(err => {
    if (err) return res.status(500).send({ msg: `Error al crear usuario: ${err}` })
    return res.status(200).send({ token: service.createToken(user) })
  })
}

/*
function signIn (req,res) {

  user.find({email:req.body.email}, (err, user)=>{
    //Si hay error, se informa
    if(err)   return res.status(500).send({message: `Error al firmarse ${err}`})
    //Se valida que no sea nulo
    if(!user) return res.status(404).send({message: `Error no existe el usuario`})
    //Si esta todo OK, se guarda en el REQUEST el usuario, se manda mensaje y el TOKEN al cliente
    req.user = return res.status(200).send({message : "Te has firmado exitosamente", token: service.createToken(user)})
  })
}
*/
const signIn = (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) return res.status(500).send({ msg: `Error con tu EMAIL : ${err}` })
    if (!user) return res.status(404).send({ msg: `no existe el Email: ${req.body.email}` })


    return user.comparePassword(req.body.password, (err, isMatch) => {
      if (err) return res.status(500).send({ msg: `Error al ingresar Password Invalido: ${err}` })
      if (!isMatch) return res.status(404).send({ msg: `Error de contraseña: ${req.body.email}` })

      req.user = user
      return res.status(200).send({ msg: 'Te has logueado correctamente', token: service.createToken(user) })
    });

  }).select('_id email +password');
}

module.exports = {
  signUp,
  signIn
}
