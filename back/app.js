'use strict'
//variables del sistema
const express = require('express'),
          app = express(),
   bodyParser = require('body-parser'),
  apiUsuarios = require('./controllers/routes-api-usuarios');

//Se interpetan los Json con bodyParser
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json())
//Se utiliza el módulo de Usarios
app.use ('/api', apiUsuarios)

//Se exporta módulo
module.exports = app
