'use strict'

const jwt = require('jwt-simple') //JSON Web Tokens
const moment = require ('moment') //para el manejo de fechas
const config = require('../config') //Se obtiene parámetro  SECRET TOKEN

//Función que genera el TOKEN
//Se va a guardar en sub el campo ID que tiene MONGO, esto no debería ser ya que se recomienda generar un ID propio
function createToken(user){
  const payload = {
    sub: user._id,
    iat: moment().unix(),  //otorga la fecha en formato UNIX
    exp: moment().add(14,'days').unix(), //El tiempo que va a caducar
  }
  //Se codifica con el payload y el secret que se configura en ../config
  return jwt.encode(payload, config.SECRET_TOKEN )
}


function decodeToken(token){
  //se crea una const decoded para crear una 'promesa'
  //resolve, que se ha cunplido la promesa
  //reject cuando no se cumplió la promesa
  const decoded = new Promise((resolve, reject) => {
    try{
      //en Payload se tendría el token decodificado, se utiliza el secret de CONFIG
      const payload = jwt.decode(token, config.SECRET_TOKEN)
      //se comprueba que el TOKEN sea válido o que no esté caducado
      if (payload.exp <= moment().unix()){
        reject({
          status  : 401,
          message : 'Token Expirado'
        })
      }
      resolve(payload.sub)
    }catch (err) {
      reject({
        status  : 500,
        message : 'Token Invalido'
      })
    }
  })
  return decoded
}

module.exports = {
  createToken,
  decodeToken
}
